package User;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import edu.esprit.pi.PMTGIT2.persistence.User;
import edu.esprit.pi.PMTGIT2.services.ProjectServiceEJBRemote;
import edu.esprit.pi.PMTGIT2.services.UserServiceEJBRemote;

public class DeleteUser {

	public static void main(String[] args) throws NamingException {
		InitialContext ctx = new InitialContext();
		ProjectServiceEJBRemote proxy1=(ProjectServiceEJBRemote)ctx.lookup("PMTGIT2-ear/PMTGIT2-ejb/ProjectServiceEJB!edu.esprit.pi.PMTGIT2.services.ProjectServiceEJBRemote");
		UserServiceEJBRemote proxy2 = (UserServiceEJBRemote )ctx.lookup("PMTGIT2-ear/PMTGIT2-ejb/UserServiceEJB!edu.esprit.pi.PMTGIT2.services.UserServiceEJBRemote");
		
		User u= (User)proxy2.findUserById(8);
		proxy2.deleteUser(u);
		System.out.println("succes");
	}

}
